﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


namespace SchetsEditor
{
    public class Schets
    {
        private Bitmap bitmap;
        private Drawer drawer;

        public Drawer GetDrawer
        {
            get { return this.drawer; }
        }

        public void Open(Stream stream)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Drawer));
            using (System.IO.StreamReader file =
            new System.IO.StreamReader(stream))
            {
                using (XmlTextReader reader = new XmlTextReader(file))
                {
                   this.drawer = (Drawer)xmlSerializer.Deserialize(reader);
                }
            }
        }

        public void Save(Stream stream)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Drawer));
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(stream))
            {
                using (XmlTextWriter xmlWriter = new XmlTextWriter(file))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    xmlWriter.Indentation = 4;
                    xmlSerializer.Serialize(xmlWriter, this.drawer);
                }
            }
        }

        public Schets()
        {
            this.drawer = new Drawer();
            bitmap = new Bitmap(1, 1);
        }

        public Graphics BitmapGraphics
        {
            get { return Graphics.FromImage(bitmap); }
        }

        public void VeranderAfmeting(Size sz)
        {
            if (sz.Width > bitmap.Size.Width || sz.Height > bitmap.Size.Height)
            {
                Bitmap nieuw = new Bitmap( Math.Max(sz.Width,  bitmap.Size.Width)
                                         , Math.Max(sz.Height, bitmap.Size.Height)
                                         );
                Graphics gr = Graphics.FromImage(nieuw);
                gr.FillRectangle(Brushes.White, 0, 0, sz.Width, sz.Height);
                gr.DrawImage(bitmap, 0, 0);
                bitmap = nieuw;
            }
        }

        public void Teken(Graphics gr)
        {
            gr.DrawImage(bitmap, 0, 0);
        }

        public void Schoon()
        {
            Graphics gr = Graphics.FromImage(bitmap);
            gr.FillRectangle(Brushes.White, 0, 0, bitmap.Width, bitmap.Height);
        }

        public void Roteer()
        {
            bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }
    }
}
