﻿using System.Drawing;
using System.Collections.Generic;
using System;
using System.Xml;

namespace SchetsEditor
{
    public interface IGraphics
    {
        void FillEllipse(Brush b, Rectangle r);
        void DrawEllipse(Pen p, Rectangle r);
        void DrawRectangle(Pen p, Rectangle r);
        void FillRectangle(Brush p, Rectangle r);
        void DrawLine(Pen p, Point start, Point end);
    }

    public class GraphicsAdapter : IGraphics
    {
        private Graphics g;
        public GraphicsAdapter(Graphics g) => this.g = g;
        public void FillEllipse(Brush b, Rectangle r) => g.FillEllipse(b, r);
        public void DrawEllipse(Pen p, Rectangle r) => g.DrawEllipse(p, r);
        public void DrawRectangle(Pen p, Rectangle r) => g.DrawRectangle(p, r);
        public void FillRectangle(Brush p, Rectangle r) => g.FillRectangle(p, r);
        public void DrawLine(Pen p, Point start, Point end) => g.DrawLine(p, start, end);
    }

    public class Drawer : IGraphics
    {
        private List<Shape> shapes;

        public Drawer()
        {
            shapes = new List<Shape>();
        }

        public void Draw(Graphics g)
        {
            foreach (var shape in shapes) shape.Draw(g);
        }

        // Removes the first shape that lies within the proximity from the point.
        public void Remove(Point target, int proximity)
        {
            foreach (var shape in shapes)
            {
                if (shape.Collides(target, proximity))
                {
                    // TODO decide on the design of remove
                    shapes.Remove(shape);
                    break;
                }
            }
        }

        // Adapter code for IGraphics.
        public void FillEllipse(Brush b, Rectangle r) => this.shapes.Add(new Disk(new Pen(b), r));
        public void DrawEllipse(Pen p, Rectangle r)=> this.shapes.Add(new Circle(p, r));
        public void DrawRectangle(Pen p, Rectangle r) => this.shapes.Add(new Rectangle2(p, r));
        public void FillRectangle(Brush p, Rectangle r)=> this.shapes.Add(new Square(new Pen(p), r));
        public void DrawLine(Pen p, Point start, Point end) => this.shapes.Add(new Line(p, start, end));
    }

    public abstract class Shape
    {
        public abstract void Draw(Graphics g);
        public abstract bool Collides(Point p, int proximity);

        // https://stackoverflow.com/questions/5514366/how-to-know-if-a-line-intersects-a-rectangle
        protected static bool SegmentIntersectRectangle(
        double rectangleMinX,
        double rectangleMinY,
        double rectangleMaxX,
        double rectangleMaxY,
        double p1X,
        double p1Y,
        double p2X,
        double p2Y)
        {
            // Find min and max X for the segment
            double minX = p1X;
            double maxX = p2X;

            if (p1X > p2X)
            {
                minX = p2X;
                maxX = p1X;
            }

            // Find the intersection of the segment's and rectangle's x-projections
            if (maxX > rectangleMaxX)
            {
                maxX = rectangleMaxX;
            }

            if (minX < rectangleMinX)
            {
                minX = rectangleMinX;
            }

            if (minX > maxX) // If their projections do not intersect return false
            {
                return false;
            }

            // Find corresponding min and max Y for min and max X we found before
            double minY = p1Y;
            double maxY = p2Y;

            double dx = p2X - p1X;

            if (System.Math.Abs(dx) > 0.0000001)
            {
                double a = (p2Y - p1Y) / dx;
                double b = p1Y - a * p1X;
                minY = a * minX + b;
                maxY = a * maxX + b;
            }

            if (minY > maxY)
            {
                double tmp = maxY;
                maxY = minY;
                minY = tmp;
            }

            // Find the intersection of the segment's and rectangle's y-projections
            if (maxY > rectangleMaxY)
            {
                maxY = rectangleMaxY;
            }

            if (minY < rectangleMinY)
            {
                minY = rectangleMinY;
            }

            if (minY > maxY) // If Y-projections do not intersect return false
            {
                return false;
            }

            return true;
        }
    }

    public class Line : Shape
    {
        protected Point start;
        protected Point end;
        protected Pen pen;

        public Line(Pen p, Point start, Point end)
        {
            this.pen = p;
            this.start = start;
            this.end = end;
        }

        public override void Draw(Graphics g)
        {
            g.DrawLine(pen, start, end);
        }

        public override bool Collides(Point p, int proximity)
        {
            var r = new Rectangle(p.X, p.Y, proximity, proximity);
            return SegmentIntersectRectangle(r.X, r.Y, r.X + r.Width, r.Y + r.Height, start.X, start.Y, end.X, end.Y);
        }
    }

    public class Rectangle2 : Shape
    {
        protected Pen pen;
        protected System.Drawing.Rectangle rect;

        public Rectangle2(Pen p, System.Drawing.Rectangle r)
        {
            this.pen = p;
            this.rect = r;
        }

        public override void Draw(Graphics g)
        {
            g.DrawRectangle(pen, rect);
        }

        public override bool Collides(Point p, int proximity)
        {
            return rect.IntersectsWith(new Rectangle(p.X, p.Y, proximity, proximity));
        }

    }

    public class Square : Rectangle2
    {
        public Square(Pen p, System.Drawing.Rectangle r) : base(p, r) { }

        public override void Draw(Graphics g)
        {
            g.FillRectangle(pen.Brush, rect);
        }
    }

    public class Circle : Shape
    {
        protected Pen pen;
        protected System.Drawing.Rectangle rect;

        public Circle(Pen p, System.Drawing.Rectangle r)
        {
            this.pen = p; this.rect = r;
        }

        public override void Draw(Graphics g)
        {
            g.DrawEllipse(pen, rect);
        }

        public override bool Collides(Point p, int proximity)
        {
            return rect.IntersectsWith(new Rectangle(p.X, p.Y, proximity, proximity));
        }
    }

    public class Disk : Circle
    {
        public Disk(Pen p, System.Drawing.Rectangle r) : base(p, r)
        {

        }

        public override void Draw(Graphics g)
        {
            g.FillEllipse(pen.Brush, rect);
        }
    }
}