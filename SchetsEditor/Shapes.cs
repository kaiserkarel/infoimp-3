﻿using System.Drawing;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace SchetsEditor
{
    public interface IGraphics
    {
        void FillEllipse(Brush b, Rectangle r);
        void DrawEllipse(Pen p, Rectangle r);
        void DrawRectangle(Pen p, Rectangle r);
        void FillRectangle(Brush p, Rectangle r);
        void DrawLine(Pen p, Point start, Point end);
    }

    public class GraphicsAdapter : IGraphics
    {
        private Graphics g;
        public GraphicsAdapter(Graphics g) => this.g = g;
        public void FillEllipse(Brush b, Rectangle r) => g.FillEllipse(b, r);
        public void DrawEllipse(Pen p, Rectangle r) => g.DrawEllipse(p, r);
        public void DrawRectangle(Pen p, Rectangle r) => g.DrawRectangle(p, r);
        public void FillRectangle(Brush p, Rectangle r) => g.FillRectangle(p, r);
        public void DrawLine(Pen p, Point start, Point end) => g.DrawLine(p, start, end);
        public void DrawString(string text, Font font, Brush brush, Point start, StringFormat format) => DrawString(text, font, brush, start, format);
    }

    [Serializable]
    public class Drawer : IGraphics
    {
        [XmlArray("Shapes"), XmlArrayItem(typeof(Shape), ElementName = "Shape")]
        public List<Shape> shapes;

        public Drawer()
        {
            shapes = new List<Shape>();
        }

        public void Draw(Graphics g)
        {
            foreach (var shape in shapes) shape.Draw(g);
        }

        // Removes the first shape that lies within the proximity from the point.
        public void Remove(Point target, int proximity)
        {
            // Loop through the list in reverse, since we want to pop off the topmost element.
            for (int i = shapes.Count-1; i >= 0; i--)
            {
                var shape = shapes[i];
                if (shape.Collides(target, proximity))
                {
                    shapes.Remove(shape);
                    break;
                }
            }
        }

        // Adapter code for IGraphics.
        public void FillEllipse(Brush b, Rectangle r) => this.shapes.Add(new Disk(new Pen(b), r));
        public void DrawEllipse(Pen p, Rectangle r)=> this.shapes.Add(new Circle(p, r));
        public void DrawRectangle(Pen p, Rectangle r) => this.shapes.Add(new Rectangle2(p, r));
        public void FillRectangle(Brush p, Rectangle r)=> this.shapes.Add(new Square(new Pen(p), r));
        public void DrawLine(Pen p, Point start, Point end) => this.shapes.Add(new Line(p, start, end));
        public void DrawString(string text, Font font, Brush brush, Point start, StringFormat format, Rectangle sz) => this.shapes.Add(new Text(text, font, brush, start, format, sz));
    }

    [Serializable]
    [XmlInclude(typeof(Line))]
    [XmlInclude(typeof(Rectangle2))]
    [XmlInclude(typeof(Square))]
    [XmlInclude(typeof(Circle))]
    [XmlInclude(typeof(Disk))]
    [XmlInclude(typeof(Text))]
    public abstract class Shape
    {
        public abstract void Draw(Graphics g);
        public abstract bool Collides(Point p, int proximity);

        // https://stackoverflow.com/questions/5514366/how-to-know-if-a-line-intersects-a-rectangle
        protected static bool SegmentIntersectRectangle(
        double rectangleMinX,
        double rectangleMinY,
        double rectangleMaxX,
        double rectangleMaxY,
        double p1X,
        double p1Y,
        double p2X,
        double p2Y)
        {
            // Find min and max X for the segment
            double minX = p1X;
            double maxX = p2X;

            if (p1X > p2X)
            {
                minX = p2X;
                maxX = p1X;
            }

            // Find the intersection of the segment's and rectangle's x-projections
            if (maxX > rectangleMaxX)
            {
                maxX = rectangleMaxX;
            }

            if (minX < rectangleMinX)
            {
                minX = rectangleMinX;
            }

            if (minX > maxX) // If their projections do not intersect return false
            {
                return false;
            }

            // Find corresponding min and max Y for min and max X we found before
            double minY = p1Y;
            double maxY = p2Y;

            double dx = p2X - p1X;

            if (System.Math.Abs(dx) > 0.0000001)
            {
                double a = (p2Y - p1Y) / dx;
                double b = p1Y - a * p1X;
                minY = a * minX + b;
                maxY = a * maxX + b;
            }

            if (minY > maxY)
            {
                double tmp = maxY;
                maxY = minY;
                minY = tmp;
            }

            // Find the intersection of the segment's and rectangle's y-projections
            if (maxY > rectangleMaxY)
            {
                maxY = rectangleMaxY;
            }

            if (minY < rectangleMinY)
            {
                minY = rectangleMinY;
            }

            if (minY > maxY) // If Y-projections do not intersect return false
            {
                return false;
            }

            return true;
        }
    }

    public class Text : Shape
    {

        [XmlElement("text")]
        public string text;

        [XmlElement("Rect")]
        public System.Drawing.Rectangle rect;

        [XmlElement("Font")]
        public XMLFont font;

        // reuse XMLPen instead of reimplementing Brush for now.
        [XmlElement("Pen")]
        public XMLPen pen;

        [XmlElement("Loc")]
        public Point loc;

        [XmlElement("format")]
        public StringFormat format;

        internal Text() { }

        public Text(string text, Font font, Brush brush, Point start, StringFormat format, Rectangle rect)
        {
            this.text = text;
            this.font = new XMLFont(font);
            this.pen = new XMLPen(new Pen(brush));
            this.loc = start;
            this.format = format;
            this.rect = rect;
        }

        public override void Draw(Graphics g)
        {
            g.DrawString(text, font.Font(), pen.Pen().Brush, loc, format);
        }

        public override bool Collides(Point p, int proximity)
        {
            return rect.IntersectsWith(new Rectangle(p.X, p.Y, proximity, proximity));
        }
    }

    [Serializable]
    public class Line : Shape
    {
        [XmlElement("Start")]
        public Point start;

        [XmlElement("End")]
        public Point end;

        [XmlElement("Pen")]
        public XMLPen pen;

        internal Line()
        {

        }

        public Line(Pen p, Point start, Point end)
        {
            this.pen = new XMLPen(p);
            this.start = start;
            this.end = end;
        }

        public override void Draw(Graphics g)
        {
            g.DrawLine(pen.Pen(), start, end);
        }

        public override bool Collides(Point p, int proximity)
        {
            var r = new Rectangle(p.X, p.Y, proximity, proximity);
            return SegmentIntersectRectangle(r.X, r.Y, r.X + r.Width, r.Y + r.Height, start.X, start.Y, end.X, end.Y);
        }
    }

    [Serializable]
    public class Rectangle2 : Shape
    {
        [XmlElement("Pen")]
        public XMLPen pen;

        [XmlElement("Rect")]
        public System.Drawing.Rectangle rect;

        public Rectangle2(Pen p, System.Drawing.Rectangle r)
        {
            this.pen = new XMLPen(p);
            this.rect = r;
        }

        internal Rectangle2()
        {

        }

        public override void Draw(Graphics g)
        {
            g.DrawRectangle(pen.Pen(), rect);
        }

        public override bool Collides(Point p, int proximity)
        {
            if (!(p.X > rect.X + proximity && 
                p.X < rect.X - proximity + rect.Width &&
                p.Y > rect.Y + proximity &&
                p.Y < rect.Y + rect.Height - proximity))
            {
                return rect.IntersectsWith(new Rectangle(p.X, p.Y, proximity, proximity));
            }
            return false;
        }

    }

    [Serializable]
    public class Square : Rectangle2
    {
        public Square(Pen p, System.Drawing.Rectangle r) : base(p, r) { }

        internal Square()
        {

        }

        public override void Draw(Graphics g)
        {
            g.FillRectangle(pen.Pen().Brush, rect);
        }
        public override bool Collides(Point p, int proximity)
        {
            return rect.IntersectsWith(new Rectangle(p.X, p.Y, proximity, proximity));
        }
    }

    [Serializable]
    public class Circle : Shape
    {
        [XmlElement("Pen")]
        public XMLPen pen;

        [XmlElement("Rect")]
        public System.Drawing.Rectangle rect;


        internal Circle()
        {

        }

        public Circle(Pen p, System.Drawing.Rectangle r)
        {
            this.pen = new XMLPen(p); this.rect = r;
        }

        public override void Draw(Graphics g)
        {
            g.DrawEllipse(pen.Pen(), rect);
        }

        public override bool Collides(Point p, int proximity)
        {
            double x = PointInElipse(p);
            if (x > 0.9 && x < 1.1) return true;
            return false;
        }

        // https://www.geeksforgeeks.org/check-if-a-point-is-inside-outside-or-on-the-ellipse/
        public double PointInElipse(Point p)
        {
            int a = rect.Width / 2;
            int b = rect.Height / 2;

            double C = (Math.Pow((p.X - (rect.X + rect.Width / 2)), 2) /
             (Math.Pow(a, 2)) +
            (Math.Pow((p.Y - (rect.Y + rect.Height / 2)), 2) /
             Math.Pow(b, 2)));

            return C;
        }
    }

    [Serializable]
    public class Disk : Circle
    {
        public Disk(Pen p, System.Drawing.Rectangle r) : base(p, r)
        {

        }

        internal Disk()
        {

        }

        public override void Draw(Graphics g)
        {
            g.FillEllipse(pen.Pen().Brush, rect);
        }

        public override bool Collides(Point p, int proximity)
        {
            if (PointInElipse(p) <= 1) return true;
            return false;
        }
    }

    [Serializable]
    public class XMLPen
    {

        [NonSerialized]
        protected Pen pen;

        [XmlElement("Color")]
        public int Color
        {
            set { pen.Color = System.Drawing.Color.FromArgb(value); }
            get { return pen.Color.ToArgb(); }
        }

        [XmlElement("Width")]
        public float Width
        {
            set { pen.Width = value;}
            get { return pen.Width; }
        }

        /// <summary>
        /// Intended for xml serialization purposes only
        /// </summary>
        public XMLPen(Pen p)
        {
            this.pen = p;
        }

        public Pen Pen() {
            return pen;
        }

        internal XMLPen()
        {
            // sane default
            this.pen = new Pen(System.Drawing.Color.Black);
        }
    }

    public class XMLFont
    {
        public string FontFamily { get; set; }
        public GraphicsUnit GraphicsUnit { get; set; }
        public float Size { get; set; }
        public FontStyle Style { get; set; }

        /// <summary>
        /// Intended for xml serialization purposes only
        /// </summary>
        private XMLFont() { }

        public XMLFont(Font f)
        {
            FontFamily = f.FontFamily.Name;
            GraphicsUnit = f.Unit;
            Size = f.Size;
            Style = f.Style;
        }

        public static XMLFont FromFont(Font f)
        {
            return new XMLFont(f);
        }

        public Font Font()
        {
            return new Font(FontFamily, Size, Style, GraphicsUnit);
        }
    }
}