---
title: INFOIMP-WC-3
author: Sjoerd Schilder 6894747, Karel Kubat, 6913466
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
---

# Aanpassingen

We verdelen de aanpassingen per opdracht die we hebben gemaakt. We focussen voornamelijk op de conceptuele aanpassingen en beschrijven niet per se elke aanpassing.

## Cirkels

De twee nieuwe tools toevoegen was relatief triviaal; de internals vn de circle (lege cirkel) en disk tool (volle cirkel) zijn als volgt:

```csharp
public class CirkelTool : TweepuntTool
{
    public override string ToString() { return "cirkel"; }

    public override void Bezig(IGraphics g, Point p1, Point p2)
    {
        g.DrawEllipse(MaakPen(kwast, 3), TweepuntTool.Punten2Rechthoek(p1, p2));
    }
}

public class DiskTool : TweepuntTool
{
    public override string ToString() { return "disk"; }

    public override void Bezig(IGraphics g, Point p1, Point p2)
    {
        g.FillEllipse(kwast, TweepuntTool.Punten2Rechthoek(p1, p2));
    }
}
```

Inspiratie hebben we gehaald uit de rectangle tools. Verder voegen we natuurlijk de tool toe aan de menu constructors:

```csharp
ISchetsTool[] deTools = { new PenTool()         
                                    , new LijnTool()
                                    , new RechthoekTool()
                                    , new VolRechthoekTool()
                                    , new TekstTool()
                                    , new CirkelTool()
                                    , new DiskTool()
                                    , new GumLayerTool()
                                    };
```

De images worden gehaald via de ToString methode van de tools. Door twee plaatjes toe te voegen genaamd cirkel en disk worden icoontjes ook mooi aangegeven (en `Properties/Resources.resx` te updaten natuurlijk).

## Het nieuwe gummen

Om volledige plaatjes (of layers) te kunnen gummen, moeten we na elke Draw de tekning opslaan in een container. Hiervoor gebruiken we een list.

De verschillende soorten plaatjes drukken we uit in classes, met een abstract class Shape als superclass:

```csharp
public abstract class Shape
{
    public abstract void Draw(Graphics g);
    public abstract bool Collides(Point p, int proximity);
}
```

De signature van de tools passen we aan; in plaats van een `Graphics` object, accepteren ze de interface IGraphics.

```csharp
public interface IGraphics
{
    void FillEllipse(Brush b, Rectangle r);
    void DrawEllipse(Pen p, Rectangle r);
    void DrawRectangle(Pen p, Rectangle r);
    void FillRectangle(Brush p, Rectangle r);
    void DrawLine(Pen p, Point start, Point end);
}

// TweepuntTool : StartpuntTool
public abstract void Bezig(IGraphics g, Point p1, Point p2);

// For completed images, we want to use the actual Drawer to store the image
// instead of the graphics adapter. 
public virtual void Compleet(IGraphics g, Point p1, Point p2)
{   this.Bezig(g, p1, p2);
}
```

Tijdens mousedrags ontvangt de tool een daadwerkelijk graphics object, zodat er direct naar het scherm getekend wordt. Echter wanneer de muis los wordt gelaten, ontvangt de tool een Drawer object.

```csharp
 public override void MuisDrag(SchetsControl s, Point p)
{   s.Refresh();
    this.Bezig(new GraphicsAdapter(s.CreateGraphics()), this.startpunt, p);
}

public override void MuisLos(SchetsControl s, Point p)
{   base.MuisLos(s, p);
    this.Compleet(s.GetDrawer, this.startpunt, p);
    s.Invalidate();
}

[Serializable]
public class Drawer : IGraphics
{
    [XmlArray("Shapes"), XmlArrayItem(typeof(Shape), ElementName = "Shape")]
    public List<Shape> shapes;

    public Drawer()
    {
        shapes = new List<Shape>();
    }

    public void Draw(Graphics g)
    {
        foreach (var shape in shapes) shape.Draw(g);
    }

    // Removes the first shape that lies within the proximity from the point.
    public void Remove(Point target, int proximity)
    {
        foreach (var shape in shapes)
        {
            if (shape.Collides(target, proximity))
            {
                shapes.Remove(shape);
                break;
            }
        }
    }

    // Adapter code for IGraphics.
    public void FillEllipse(Brush b, Rectangle r) => this.shapes.Add(new Disk(new Pen(b), r));
    public void DrawEllipse(Pen p, Rectangle r)=> this.shapes.Add(new Circle(p, r));
    public void DrawRectangle(Pen p, Rectangle r) => this.shapes.Add(new Rectangle2(p, r));
    public void FillRectangle(Brush p, Rectangle r)=> this.shapes.Add(new Square(new Pen(p), r));
    public void DrawLine(Pen p, Point start, Point end) => this.shapes.Add(new Line(p, start, end));
    public void DrawString(string text, Font font, Brush brush, Point start, StringFormat format, Rectangle sz) => this.shapes.Add(new Text(text, font, brush, start, format, sz));
}
```

De Drawer slaat de shape op in de list. We gebruiken een adapter wanneer we `Graphics` objecten die IGraphics implementeren nodig hebben.

```csharp
public class GraphicsAdapter : IGraphics
{
    private Graphics g;
    public GraphicsAdapter(Graphics g) => this.g = g;
    public void FillEllipse(Brush b, Rectangle r) => g.FillEllipse(b, r);
    public void DrawEllipse(Pen p, Rectangle r) => g.DrawEllipse(p, r);
    public void DrawRectangle(Pen p, Rectangle r) => g.DrawRectangle(p, r);
    public void FillRectangle(Brush p, Rectangle r) => g.FillRectangle(p, r);
    public void DrawLine(Pen p, Point start, Point end) => g.DrawLine(p, start, end);
    public void DrawString(string text, Font font, Brush brush, Point start, StringFormat format) => DrawString(text, font, brush, start, format);
}
```

Elke instantie van `Schets` heeft een `Drawer` object. Wanneer de `Invalidate` methode van SchetsEditor wordt aangeroepen, zorgen we ervoor dat `Drawer.Draw` ook wordt aangeroepen:

```csharp
// public class SchetsControl : UserControl
public new void Invalidate()
{
    this.GetDrawer.Draw(this.MaakBitmapGraphics());
    base.Invalidate();
}
```

Kortweg, tijdens slepen wordt direct getekend, maar wanneer de muis los wordt gelaten, wordt de shape toegevoegd aan een Drawer object met een interne container. Tools zijn zich niet bewust wat de draw target is.

Om het gummen te implementeren, gebruiken we de `Collides` methode van Shape. Deze krijgt de muislocatie, en de proximity (onze configuratie om te bepalen of de muis dichtbij genoeg is om een geldige click te geven).

Voor de meeste figuren is de implementatie vrij simpel: 

```csharp
public override bool Collides(Point p, int proximity)
{
    return rect.IntersectsWith(MouseBorder(p, proximity));
}
```

We maken een rectangle van de muisklik, en kijken of deze intersect met het figuur. Voor lege vormen en cirkels is deze wat uitgebreider.

```csharp
// non-filled rectangle
public override bool Collides(Point p, int proximity)
{
    if (!(p.X > rect.X + proximity && 
        p.X < rect.X - proximity + rect.Width &&
        p.Y > rect.Y + proximity &&
        p.Y < rect.Y + rect.Height - proximity))
    {
        return rect.IntersectsWith(new Rectangle(p.X, p.Y, proximity, proximity));
    }
    return false;
}

// non-filled square
public override bool Collides(Point p, int proximity)
{
    double x = PointInElipse(p);
    if (x > 0.9 && x < 1.1) return true;
    return false;
}

// https://www.geeksforgeeks.org/check-if-a-point-is-inside-outside-or-on-the-ellipse/
public double PointInElipse(Point p)
{
    int a = rect.Width / 2;
    int b = rect.Height / 2;

    double C = (Math.Pow((p.X - (rect.X + rect.Width / 2)), 2) /
     (Math.Pow(a, 2)) +
    (Math.Pow((p.Y - (rect.Y + rect.Height / 2)), 2) /
     Math.Pow(b, 2)));

    return C;
}
```

Wanneer de gum collides met het object verwijderen we deze uit de lijst:

```csharp
public void Remove(Point target, int proximity)
{
    foreach (var shape in shapes)
    {
        if (shape.Collides(target, proximity))
        {
            shapes.Remove(shape);
            break;
        }
    }
}

public class GumLayerTool : StartpuntTool
{
    public override string ToString() { return "gumlayer"; }
    public override void MuisVast(SchetsControl s, Point p) { }
    public override void MuisDrag(SchetsControl s, Point p) { }
    public override void Letter(SchetsControl s, char c) { }
    public override void MuisLos(SchetsControl s, Point p)
    {
        s.GetDrawer.Remove(p, 8);
        s.Schets.Schoon();
        s.GetDrawer.Draw(s.MaakBitmapGraphics());
        s.Invalidate();
    }
}
```

Voor de tekst componenten slaan we per letter de omvangende rectangle op, en gebruiken we die rectangle voor de collision detection. Een polyline slaan we op en gummen we d.v.m. de individuele lijnstukken. Het gummen van een polyline is dus vrij vervelend.

## Het nieuwe Opslaan en teruglezen

Voor opslaan en teruglezen gebruiken we XML serializatie. We hebben onze Shape subclasses geannoteerd met Xml tags:

```csharp
[Serializable]
[XmlInclude(typeof(Line))]
[XmlInclude(typeof(Rectangle2))]
[XmlInclude(typeof(Square))]
[XmlInclude(typeof(Circle))]
[XmlInclude(typeof(Disk))]
[XmlInclude(typeof(Text))]
public abstract class Shape
...
```

Om Pens en Fonts te kunnen serializen, hebben we DTO classes geschreven:

```csharp
[Serializable]
public class XMLPen
{

    [NonSerialized]
    protected Pen pen;

    [XmlElement("Color")]
    public int Color
    {
        set { pen.Color = System.Drawing.Color.FromArgb(value); }
        get { return pen.Color.ToArgb(); }
    }

    [XmlElement("Width")]
    public float Width
    {
        set { pen.Width = value;}
        get { return pen.Width; }
    }

    /// <summary>
    /// Intended for xml serialization purposes only
    /// </summary>
    public XMLPen(Pen p)
    {
        this.pen = p;
    }

    public Pen Pen() {
        return pen;
    }

    internal XMLPen()
    {
        // sane default
        this.pen = new Pen(System.Drawing.Color.Black);
    }
}

public class XMLFont
{
    public string FontFamily { get; set; }
    public GraphicsUnit GraphicsUnit { get; set; }
    public float Size { get; set; }
    public FontStyle Style { get; set; }

    /// <summary>
    /// Intended for xml serialization purposes only
    /// </summary>
    private XMLFont() { }

    public XMLFont(Font f)
    {
        FontFamily = f.FontFamily.Name;
        GraphicsUnit = f.Unit;
        Size = f.Size;
        Style = f.Style;
    }

    public static XMLFont FromFont(Font f)
    {
        return new XMLFont(f);
    }

    public Font Font()
    {
        return new Font(FontFamily, Size, Style, GraphicsUnit);
    }
}
``` 

XML deserialization heeft per class een parameterloze constructor nodig, en zet vervolgens de waardes via public setters.

De Drawer class schrijven we naar een stream:

```csharp
public void Save(Stream stream)
{
    XmlSerializer xmlSerializer = new XmlSerializer(typeof(Drawer));
    using (System.IO.StreamWriter file =
    new System.IO.StreamWriter(stream))
    {
        using (XmlTextWriter xmlWriter = new XmlTextWriter(file))
        {
            xmlWriter.Formatting = Formatting.Indented;
            xmlWriter.Indentation = 4;
            xmlSerializer.Serialize(xmlWriter, this.drawer);
        }
    }
}
```

Het stream object ontvangen we van een Savefiledialog.

```csharp
private void opslaan(object obj, EventArgs ea)
{
    Stream stream;
    SaveFileDialog saveFileDialog = new SaveFileDialog();

    saveFileDialog.Filter = "XML files|*.xml";
    saveFileDialog.RestoreDirectory = true;

    if (saveFileDialog.ShowDialog() == DialogResult.OK)
    {
        if ((stream = saveFileDialog.OpenFile()) != null)
        {
            this.schetscontrol.Schets.Save(stream);
        }
    }
}
```

Openen gaat soortgelijk:

```csharp
private void open(object obj, EventArgs ea)
{
    Stream stream;
    OpenFileDialog openFileDialog = new OpenFileDialog();

    openFileDialog.Filter = "XML files|*.xml";
    openFileDialog.RestoreDirectory = true;

    if (openFileDialog.ShowDialog() == DialogResult.OK)
    {
        if ((stream = openFileDialog.OpenFile()) != null)
        {
            this.schetscontrol.Schets.Open(stream);
        }
    }
    this.schetscontrol.Invalidate();
}

...
public void Open(Stream stream)
{
    XmlSerializer xmlSerializer = new XmlSerializer(typeof(Drawer));
    using (System.IO.StreamReader file =
    new System.IO.StreamReader(stream))
    {
        using (XmlTextReader reader = new XmlTextReader(file))
        {
           this.drawer = (Drawer)xmlSerializer.Deserialize(reader);
        }
    }
}
```

## Extra: kleur-control

De gebruiker veel kleuren laten selecteren kan makkelijk via de built-in colour dialog.

```csharp
private void addColour(object o, EventArgs e)
{
    var dialog = new ColorDialog();
    dialog.ShowDialog();
    schetscontrol.VeranderKleur(dialog.Color);
}

...
private void maakAktieMenu(String[] kleuren)
{   
    ToolStripMenuItem menu = new ToolStripMenuItem("Aktie");
    menu.DropDownItems.Add("Clear", null, schetscontrol.Schoon );
    menu.DropDownItems.Add("Roteer", null, schetscontrol.Roteer );
    colourMenu = new ToolStripMenuItem("Kies kleur");
    foreach (string k in kleuren)
        colourMenu.DropDownItems.Add(k, null, schetscontrol.VeranderKleurViaMenu);
    colourMenu.DropDownItems.Add("More", null, addColour);
    menu.DropDownItems.Add(colourMenu);
    menuStrip.Items.Add(menu);
}
```
